# Teste para Desenvolvedor Júnior em React Native

[![e-Precise Logo](https://www.e-precise.com.br/assets/images/logo_com_sombra.png)](https://www.e-precise.com.br/)

### Visão Geral

App desenvolvido em React-Native de gestão de pessoas com listagem e cadastro de pessoas. Foi testado apenas na sua versão Android.

### Screenshots

https://drive.google.com/drive/folders/1LIHngTEJkGTiEGuKo0VbM_oCyE7uOODm?usp=sharing

### Requisitos para instalação e execução do projeto

- Git instalado
- Ambiente React-Native devidamente configurado. (Caso não tenha, minha sugestão é acessar uma documentação feita pela rocketseat: https://react-native.rocketseat.dev. Lá é possível configurar o ambiente para Windows, Linux e MacOS) -  OBS: O projeto foi desenvolvido utilizando Expo, o que nos deixa isentos da necessidade de instalação do SDK Android, opcionalmente você pode seguir os passos de configuração do ambiente apenas até a parte onde se instala o Node, NPM e Java.
- Instalar expo-cli:
  ```
  npm install -g expo-cli
  ```
  ou
  ```
  yarn global add expo-cli
  ```
- Possuir o APP Expo Go instalado:
  Android: https://play.google.com/store/apps/details?id=host.exp.exponent ou
  IOS: https://search.itunes.apple.com/WebObjects/MZContentLink.woa/wa/link?path=apps/exponent

## Executando

- Primeiro execute o comando abaixo em um diretório de sua preferência para clonar o repositório:
  ```
  git clone https://wendelllucena@bitbucket.org/wendelllucena/teste-react-junior.git
  ```
- Acesse a pasta teste-rect-junior e execute o comando abaixo para instalar as dependências:
  ```
  npm install
  ```
  ou
  ```
  yarn install
  ```
- Após a instalação de todas as dependências e ter todos os requisitos cumpridos, conecte seu dispositivo no computador através do cabo usb, ou se estiver utilizando um emulador, execute-o. Após isso, execute o comando abaixo ainda dentro da pasta do projeto teste-react-native:
  ```
  npm start
  ```
  ou
  ```
  yarn start
  ```
- Irá abrir automaticamente uma guia no seu navegador padrão, onde se encontram ferramentas do desenvolvedor, como o console e status do Metro Bundler. Também há ações para executar o APP nos dispositivos. Selecione 'Run on Android device/emulator'. Se tudo ocorrer bem e você não receber nenhuma mensagem de erro, a instalação ocorreu corretamente, caso contrário, verifique os requisitos e tente novamente.
- Com a aplicação aberta você já pode testar algumas funcionalidades como Cadastrar uma nova Pessoa, e ver as estatísticas nos gráficos mudarem na página Home.
## Facilidade encontradas no projeto
- A construção de gráficos utilizando a biblioteca react-native-chat-kit. Embora sejam meus primeiros gráficos construídos para um App em React-Native, gostei muito de implementá-los.
- Criação de componentes customizados.
- Funcionalidades básicas do projeto.
## Dificuldades encontradas no projeto
- Trabalhar com Typescript. Confesso que foi um desafio, pois nunca havia desenvolvido com react-Native utilizando Typescript e utilizando os tipos corretamente.
- Por não ter tido experiências anteriores, testes unitários foram um desafio para mim. Porém, decidi encarar o desafio e dar o meu melhor, pois este projeto me motivou a querer aprender mais sobre testes.
- No início tive dúvidas sobre como iniciar o projeto, no fim das contas decidi utilizar o expo, por ser utilizado em aplicações simples e rápidas.



## Contato e Informações
wendelllucenasi@gmail.com
