module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    env: {
      production: {
        plugins: ['react-native-paper/babel'] // reescreve as importações e adiciona no projeto apenas o que for usado.
      }
    }
  };
};
