import { useNavigation, useRoute } from '@react-navigation/core'
import React, { useEffect, useState } from 'react'
import { SafeAreaView, ScrollView, StyleSheet, ActivityIndicator, Alert } from 'react-native'
import { FAB, List } from 'react-native-paper'
import { MaterialIcons } from '@expo/vector-icons'

import Header from '../components/Header'
import { database } from '../database/database'
import { BorderlessButton } from 'react-native-gesture-handler'

export default function PersonDetails() {

  const route = useRoute()
  const navigation = useNavigation()

  const params: any = route.params
  const [isLoading, setIsLoading] = useState(false)
  const [data, setData] = useState({
    id: 0,
    name: "",
    age: "",
    cpf: "",
    rg: "",
    gender: "",
    zip_code: "",
    address_line: "",
    number: "",
    neighborhood: "",
    city: "",
    uf: "",
  })

  useEffect(() => {
    getPersonData()
  }, [])

  function getPersonData() {
    setIsLoading(true)
    database.getPersonById(params["personId"]).then((data: any) => {
      setData(data)
      setIsLoading(false)
    })
    .catch(error => {
      setIsLoading(false)
    })
  }

  function editPerson() {
    navigation.navigate('PersonForm', data)
  }

  function deletePerson() {
    Alert.alert(
      'Deletar',
      'Você tem certeza que deseja deletar essa pessoa?',
      [
        {
          text: 'Sim, deletar',
          onPress: () => {
            database.deletePersonById(data.id).then((data: any) => {
              navigation.navigate('ListPeople')
            })
            .catch(error => {
              console.log(error)
              Alert.alert('Erro!', 'Falha ao deletar pessoa. Tente novamente.')
            })
          }
        },
        {
          text: 'Não',
          onPress: () => {
            
          }
        }
      ],
      { cancelable: true }
    )
  }

  return (
    <>
    <Header 
      title="Detalhes"
      rightButton={
        <BorderlessButton onPress={deletePerson}>
          <MaterialIcons name="delete-forever" size={26} color="#FFF" />
        </BorderlessButton>
      }
    />
    <SafeAreaView style={styles.container}>
      { isLoading ? 
        <ActivityIndicator size="large" color="#1EBBFF" /> :
        <ScrollView>
        <List.Item
          style={styles.listItem}
          titleStyle={styles.title}
          descriptionStyle={styles.description}
          title="Nome"
          description={data.name}
          left={props => <List.Icon {...props} icon="account" color="#1EBBFF" />}
        />
        <List.Item
          style={styles.listItem}
          titleStyle={styles.title}
          descriptionStyle={styles.description}
          title="Idade"
          description={data.age}
          left={props => <List.Icon {...props} icon="calendar" color="#1EBBFF" />}
        />
        <List.Item
          style={styles.listItem}
          titleStyle={styles.title}
          descriptionStyle={styles.description}
          title="CPF"
          description={data.cpf}
          left={props => <List.Icon {...props} icon="card-account-details" color="#1EBBFF" />}
        />
        <List.Item
          style={styles.listItem}
          titleStyle={styles.title}
          descriptionStyle={styles.description}
          title="RG"
          description={data.rg}
          left={props => <List.Icon {...props} icon="card-account-details" color="#1EBBFF" />}
        />
        <List.Item
          style={styles.listItem}
          titleStyle={styles.title}
          descriptionStyle={styles.description}
          title="Gênero"
          description={data.gender}
          left={props => <List.Icon {...props} icon="gender-male-female" color="#1EBBFF" />}
        />
        <List.Item
          style={styles.listItem}
          titleStyle={styles.title}
          descriptionStyle={styles.description}
          title="CEP"
          description={data.zip_code}
          left={props => <List.Icon {...props} icon="map-marker" color="#1EBBFF" />}
        />
        <List.Item
          style={styles.listItem}
          titleStyle={styles.title}
          descriptionStyle={styles.description}
          title="Rua"
          description={data.address_line}
          left={props => <List.Icon {...props} icon="map-marker" color="#1EBBFF" />}
        />
        <List.Item
          style={styles.listItem}
          titleStyle={styles.title}
          descriptionStyle={styles.description}
          title="Número"
          description={data.number}
          left={props => <List.Icon {...props} icon="map-marker" color="#1EBBFF" />}
        />
        <List.Item
          style={styles.listItem}
          titleStyle={styles.title}
          descriptionStyle={styles.description}
          title="Bairro"
          description={data.neighborhood}
          left={props => <List.Icon {...props} icon="map-marker" color="#1EBBFF" />}
        />
        <List.Item
          style={styles.listItem}
          titleStyle={styles.title}
          descriptionStyle={styles.description}
          title="Cidade"
          description={data.city}
          left={props => <List.Icon {...props} icon="map-marker" color="#1EBBFF" />}
        />
        <List.Item
          style={styles.listItem}
          titleStyle={styles.title}
          descriptionStyle={styles.description}
          title="UF"
          description={data.uf}
          left={props => <List.Icon {...props} icon="map-marker" color="#1EBBFF" />}
        />
        
      </ScrollView>}
      { !isLoading &&
        <FAB
          style={styles.fab}
          color="#FFF"
          icon="square-edit-outline"
          onPress={editPerson}
        /> }
    </SafeAreaView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 20,
    bottom: 20,
    backgroundColor: "#1EBBFF"
  },
  listItem: {
    marginTop: 10,
    backgroundColor: "#fdf6f6",
    borderRadius: 12
  },
  title: {
    color: "#1EBBFF",
    fontSize: 15,
    fontWeight: 'bold',
  },
  description: {
    fontSize: 16,
    color: "#000",
    marginTop: 5
  }
})