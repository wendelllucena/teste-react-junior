import { useIsFocused } from '@react-navigation/core'
import React, { useEffect, useState } from 'react'
import { Alert, StyleSheet, Text as TextField, View, Dimensions } from 'react-native'
import { ActivityIndicator, Card, Title } from 'react-native-paper'
import { StackedBarChart, PieChart } from 'react-native-chart-kit'

import { database } from '../database/database'
import Header from '../components/Header'
import { ScrollView } from 'react-native-gesture-handler'


export default function Home() {

  const isFocused = useIsFocused()
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState([])
  const [ageRange, setAgeRange] = useState([[], [], []])
  const [genderAmount, setGenderAmount] = useState([
    {
      name: 'M',
      population: 0,
      color: "#4450fc",
      legendFontColor: "#000000",
      legendFontSize: 15,
    },
  {
    name: 'F',
    population: 0,
    color: "#1EBBFF",
      legendFontColor: "#000000",
      legendFontSize: 15,
  },
  {
    name: 'Outro',
    population: 0,
    color: "#065476",
      legendFontColor: "#000000",
      legendFontSize: 15,
  }
  ])

  const screenWidth = Dimensions.get('screen').width

  useEffect(() => {
    getData()
  }, [isFocused])


  function getData() {
    setLoading(true)
    database.getPersonList().then((data: any) => {
      let range1 = 0
      let range2 = 0
      let range3 = 0

      getGenderAmount(data)

      if (data.length > 0) {
        data.map((value: any, index: number, array: any[]) => {
          let age = parseInt(value.age)
          if (age < 35) {
            range1 += 1
          } else if (age > 35 && age < 60) {
            range2 += 1
          } else {
            range3 += 1
          }
        })

        const ageRange: any = [range1 ? [range1] : [], range2 ? [range2] : [], range3 ? [range3] : []]

        setAgeRange(ageRange)
      } else {
        setAgeRange([[], [], []])
      }

      setData(data)
      setLoading(false)
    })
    .catch(error => {
      console.log('get age ranges error: ', error)
      Alert.alert('Erro!', 'Falha ao listar dados.')
      setLoading(false)
    })
  }

  function getGenderAmount(data: any[]) {
    let maleList = []
    let femaleList = []
    let otherList = []

    maleList = data.filter((value: any, index: number, array: any[]) => {
      return value.gender === 'Masculino'
    })

    femaleList = data.filter((value: any, index: number, array: any[]) => {
      return value.gender === 'Feminino'
    })

    otherList = data.filter((value: any, index: number, array: any[]) => {
      return value.gender === 'Outro'
    })



    setGenderAmount([
      {
        name: 'Masculino',
        population: maleList.length,
        color: "#4450fc",
        legendFontColor: "#000000",
        legendFontSize: 15,
      },
    {
      name: 'Feminino',
      population: femaleList.length,
      color: "#1EBBFF",
        legendFontColor: "#000000",
        legendFontSize: 15,
    },
    {
      name: 'Outro',
      population: otherList.length,
      color: "#065476",
        legendFontColor: "#000000",
        legendFontSize: 15,
    }    
    ])
  }

  const chartData = {
    labels: ["18 a 35 anos", "36 a 60 anos", "mais de 60 anos"],
    legend: ["18 a 35 anos", "36 a 60 anos", "mais de 60 anos"],
    data: ageRange,
    barColors: [
      "#d5d6dc",
      "#c7c8c9",
      "#abacae"]
  }


  return (
    <>
    <Header title="Home" menu={true} />
    <ScrollView>
      <View style={styles.container}>
      <Card style={styles.ageChar} elevation={2}>
          <Title style={styles.infoTitle}>Faixa etária de pessoas cadastradas</Title>
          {loading ? <ActivityIndicator size="small" color="#1EBBFF" /> :
            <StackedBarChart 
              data={chartData}
              width={screenWidth - 40}
              height={200}
              hideLegend={true}
              style={{ borderRadius: 3 }}
              decimalPlaces={0}
              chartConfig={{
                backgroundColor: "#009ae2",
                backgroundGradientFrom: "#00abfb",
                backgroundGradientTo: "#146df3",
                decimalPlaces: 2, // optional, defaults to 2dp
                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                propsForDots: {
                  r: "6",
                  strokeWidth: "2",
                  stroke: "#26b3ff"
                }
              }}
            />
          }
        </Card>
        
        <Card style={styles.ageChar} elevation={2}>
          <Title style={styles.infoTitle}>% de gêneros cadastrados</Title>
          { loading ? <ActivityIndicator size="small" color="#1ebbFF" /> :
            <PieChart
              width={screenWidth - 40}
              height={150}
              paddingLeft={"5"}
              data={genderAmount}
              accessor={"population"}
              backgroundColor={"transparent"}
              chartConfig={{
                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                strokeWidth: 2,
                barPercentage: 1,

              }}
            />
          }
        </Card>

        <View style={styles.cardContainer}>
          <Card style={styles.card} elevation={2}>
            <Title style={styles.infoTitle}>Total de cadastros</Title>
            { loading ? <ActivityIndicator size="small" color="#1EBBFF" /> : <TextField style={styles.infoText}>{ data.length }</TextField>}
          </Card>
        </View>
      </View>
    </ScrollView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  cardContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  card: {
    width: '45%',
    height: 100,
    margin: 10,
    padding: 10,
    paddingVertical: 10,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center'
  },
  infoTitle: {
    textAlign: 'center',
    fontSize: 18
  },
  infoText: {
    textAlign: 'center',
    color: "#1EBBFF",
    fontSize: 20
  },
  ageChar: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    padding: 10,
  }
})