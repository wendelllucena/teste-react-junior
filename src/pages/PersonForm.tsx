import React, { useState } from 'react'
import { useNavigation, useRoute } from '@react-navigation/core'
import { SafeAreaView, ScrollView, StyleSheet, Text, View, Alert, ActivityIndicator } from 'react-native'
import { Picker } from '@react-native-picker/picker'
import { Formik } from 'formik'
import { Button } from 'react-native-paper'
import { object, string, number } from 'yup'

import TextField from '../components/TextField'
import { database } from '../database/database'
import { PersonModel } from '../models/person.model'

import { cepApi } from  '../services/location.service'
import Header from '../components/Header'


export default function PersonForm() {

  const [isLoading, setIsLoading] = useState(false)
  const [zipCodeIsLoading, setZipCodeIsLoading] = useState(false)
  const [cepError, setCepError] = useState('')
  const navigation = useNavigation()
  const route = useRoute()
  const params: any = route.params

  let initialFormValues: PersonModel = {
    name: params ? params['name'] : '',
    age: params ? params['age'] : '',
    cpf: params ? params['cpf'] : '',
    rg: params ? params['rg'] : '',
    gender: params ? params['gender'] : '',
    zip_code: params ? params['zip_code'] : '',
    address_line: params ? params['address_line'] : '',
    number: params ? params['number'] : '',
    neighborhood: params ? params['neighborhood'] : '',
    city: params ? params['city'] : '',
    uf: params ? params['uf'] : '',
  }

  function savePerson(values: PersonModel) {
    setIsLoading(true)
    database.insertPerson(values).then(data => {
      setIsLoading(false)
      Alert.alert(
        'Sucesso!',
        'Pessoa cadastrada com sucesso!',
        [{
          text: 'Ok',
          onPress: () => navigation.navigate('ListPeople')
        }]
      )
    })
    .catch(error => {
      Alert.alert('Erro!', 'Falha ao cadastrar pessoa. Tente novamente.')
      console.log(error)
      setIsLoading(false)
    })
  }

  function updatePerson(values: any) {
    setIsLoading(true)
    database.updatePerson(values, params['id']).then(data => {
      setIsLoading(false)
      Alert.alert(
        'Sucesso!',
        'Dados atualizados com sucesso!',
        [{
          text: 'Ok',
          onPress: () => navigation.navigate('ListPeople')
        }]
      )
    })
    .catch(error => {
      Alert.alert('Erro!', 'Falha ao atualizar dados. Tente novamente.')
      console.log(error)
      setIsLoading(false)
    })
  }

  const FormSchema = object().shape({
    name: string().min(5).required('Nome é obrigatório'),
    age: number().min(18, 'Apenas maiores de idade podem se inscrever').required('Idade é obrigatório'),
    cpf: string().min(14, 'Digite todos os números do CPF').required('CPF é um campo obrigatório'),
    rg: string().required('RG é um campo obrigatório'),
    gender: string().required('O campo gênero é obrigatório'),
    zip_code: string().min(10, 'CEP inválido').required('CEP é um campo obrigatório'),
    address_line: string().required('Rua é um campo obrigatório'),
    number: number().required('Número é um campo obrigatório'),
    neighborhood: string().required('Bairro é um campo obrigatório'),
    city: string().required('Cidade é um campo obrigatório'),
    uf: string().required('UF é um campo obrigatório'),
  })
  
  return (
    <>
    <Header title={params ? 'Atualizar cadastro' : 'Cadastrar pessoa'} />
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <Formik  
          initialValues={initialFormValues}
          onSubmit={(values: PersonModel) => {
            params ? updatePerson(values) : savePerson(values)
          }}
          validationSchema={FormSchema}
        >
          {({ handleChange, handleSubmit, values, errors, touched, setFieldTouched, setFieldValue }) => (
            <>
              <View style={styles.divider}>
                <Text style={styles.dividerText}>Informações Pessoais</Text>
              </View>

              <TextField
                name="name"
                label="Nome"
                placeholder="Digite seu nome"
                value={values.name}
                onChangeText={handleChange('name')}
                onBlur={() => setFieldTouched('name', true)}
              />
              { errors.name && touched.name && <Text style={styles.errorMessage}>{errors.name}</Text> }
              
              <TextField
                name="age"
                label="Idade"
                placeholder="Digite sua idade"
                value={values.age.toString()}
                keyboardType={"numeric"}
                onChangeText={handleChange('age')}
                onBlur={() => setFieldTouched('age', true)}
              />
              { errors.age && touched.age && <Text style={styles.errorMessage}>{errors.age}</Text> }
        
              <TextField
                name="cpf"
                label="CPF"
                placeholder="Digite seu CPF"
                value={values.cpf}
                mask="999.999.999-99"
                keyboardType="numeric"
                onChangeText={handleChange('cpf')}
                onBlur={() => setFieldTouched('cpf', true)}
              />
              { errors.cpf && touched.cpf && <Text style={styles.errorMessage}>{errors.cpf}</Text> }

              <TextField
                name="rg"
                label="RG"
                placeholder="Digite seu RG"
                value={values.rg}
                keyboardType={"numeric"}
                onChangeText={handleChange('rg')}
                onBlur={() => setFieldTouched('rg', true)}
              />
              { errors.rg && touched.rg && <Text style={styles.errorMessage}>{errors.rg}</Text> }

              <Text style={styles.labelPicker}>Gênero</Text>
              <View style={styles.picker}>
                <Picker
                  mode="dropdown"
                  selectedValue={values.gender}
                  onValueChange={(itemValue, itemIndex) => {
                    setFieldValue('gender', itemValue)
                  }}
                  >
                    <Picker.Item label="Masculino" value="Masculino" />
                    <Picker.Item label="Feminino" value="Feminino" />
                    <Picker.Item label="Outro" value="Outro" />
                </Picker>
              </View>
              { errors.gender && touched.gender && <Text style={styles.errorMessage}>{errors.gender}</Text> }
        
              <View style={styles.divider}> 
                <Text style={styles.dividerText}>Informações de Endereço</Text>
              </View>

              
              <TextField
                name="zip_code"
                label="CEP"
                placeholder="Digite seu CEP"
                value={values.zip_code}
                mask="99.999-999"
                keyboardType={"numeric"}
                onChangeText={async text => {
                  handleChange('zip_code')(text)
                  if (text.length === 10) {
                    setZipCodeIsLoading(true)
                    let zip_code = text.replace('.', '').replace('-', '')
                    cepApi(zip_code).then((address: any) => {
                      handleChange('address_line')(address.logradouro)
                      handleChange('neighborhood')(address.bairro)
                      handleChange('city')(address.localidade)
                      handleChange('uf')(address.uf)
                      setZipCodeIsLoading(false)
                      setCepError('')
                    })
                    .catch(error => {
                      setCepError(error)
                      setZipCodeIsLoading(false)
                    })
                  }
                }}
                onBlur={() => setFieldTouched('zip_code', true)}
              />
              { zipCodeIsLoading && <ActivityIndicator size="small" color="#1EBBFF" />  }
              { cepError ? <Text style={styles.errorMessage}>Não foi possível localizar o cep.</Text> : <></> }
              { errors.zip_code && touched.zip_code && <Text style={styles.errorMessage}>{errors.zip_code}</Text> }

              <TextField
                name="address_line"
                label="Rua"
                placeholder="Nome da Rua"
                value={values.address_line}
                onChangeText={handleChange('address_line')}
                onBlur={() => setFieldTouched('address_line', true)}
              />
              { errors.address_line && touched.address_line && <Text style={styles.errorMessage}>{errors.address_line}</Text> }
              
              <TextField
                name="number"
                label="Número"
                placeholder="Número"
                value={values.number.toString()}
                keyboardType={"numeric"}
                onChangeText={handleChange('number')}
                onBlur={() => setFieldTouched('number', true)}
              />
              { errors.number && touched.number && <Text style={styles.errorMessage}>{errors.number}</Text> }
              
              <TextField
                name="neighborhood"
                label="Bairro"
                placeholder="Nome do Bairro"
                value={values.neighborhood}
                onChangeText={handleChange('neighborhood')}
                onBlur={() => setFieldTouched('neighborhood', true)}
              />
              { errors.neighborhood && touched.neighborhood && <Text style={styles.errorMessage}>{errors.neighborhood}</Text> }
              
              <TextField
                name="city"
                label="Cidade"
                placeholder="Nome da Cidade"
                value={values.city}
                onChangeText={handleChange('city')}
                onBlur={() => setFieldTouched('city', true)}
              />
              { errors.city && touched.city && <Text style={styles.errorMessage}>{errors.city}</Text> }
              
              <TextField
                name="uf"
                label="UF"
                placeholder="UF do Estado"
                value={values.uf}
                autoCapitalize="characters"
                onChangeText={handleChange('uf')}
                onBlur={() => setFieldTouched('uf', true)}
              />
              { errors.uf && touched.uf && <Text style={styles.errorMessage}>{errors.uf}</Text> }
            

              <Button style={styles.submitButton} loading={isLoading} color="#6d6868" icon="send" mode="contained" onPress={handleSubmit}>
                { params ? 'Atualizar' : 'Salvar' }
              </Button>
            </>
          )}
        </Formik>
      </ScrollView>
      </SafeAreaView>
      </>
)}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  divider: {
    padding: 15,
    backgroundColor: "#1EBBFF",
    borderRadius: 8,
    marginTop: 20,
    marginBottom: 10
  },
  dividerText: {
    color: "#FFF",
    fontSize: 18,
    fontWeight: 'bold'
  },
  submitButton: {
    marginVertical: 30,
    padding: 10,
    borderRadius: 10,
  },
  errorMessage: {
    marginLeft: 5,
    marginBottom: 5,
    color: "#F33333"
  },
  updateTitleContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  updateTitleText: {
    color: "#000",
    fontSize: 20,
    fontWeight: 'bold'
  },
  labelPicker: {
    paddingVertical: 2,
    fontSize: 16,
    fontWeight: 'bold',
    color: "#c0cbd3"
  },
  picker: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#c0cbd3',
    height: 50,
    color: '#000000'
  }
})