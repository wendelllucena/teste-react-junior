import { useNavigation, useIsFocused } from '@react-navigation/core'
import React, { useEffect, useState } from 'react'
import { StyleSheet, SafeAreaView, ScrollView, Text, ActivityIndicator, Alert } from 'react-native'
import { Card, FAB } from 'react-native-paper'
import { database } from '../database/database'

import PersonCard from '../components/PersonCard'
import Header from '../components/Header'


export default function ListPeople() {

  const navigation = useNavigation()
  const [personList, setPersonList] = useState([])
  const [loading, setLoading] = useState(false)

  const isFocused = useIsFocused()

  useEffect(() => {
    getData()
  }, [isFocused])


  function getData() {
    setLoading(true)
    database.getPersonList().then((data: any) => {      
      setPersonList(data)
      setLoading(false)
    })
    .catch(error => {
      console.log(error)
      Alert.alert('Erro!', 'Falha ao listar pessoas.')
      setLoading(false)
    })
  }

  function goToCreateNewPerson() {
    navigation.navigate('PersonForm')
  }


  return (
    <>
      <Header title="Pessoas" menu={true} />
      <SafeAreaView style={styles.container}>
        <ScrollView>
          { personList.length < 1 &&  
            <Card style={styles.emptyDataCard}>
              <Text style={styles.emptyDataText}>Não há pessoas cadastradas. Você pode cadastrar clicando no botão abaixo.</Text>
            </Card>
          }
          { loading && !personList && <ActivityIndicator size="large" color="#1EBBFF" /> }
          { personList ? personList.map((pessoa: any, index) => {
            return (
              
              <PersonCard key={index} id={pessoa.id} name={pessoa.name} age={pessoa.age} cpf={pessoa.cpf} />
            )
          }) : <Text>Teste</Text> }
        </ScrollView>
        <FAB
          style={styles.fab}
          icon="plus"
          color="#FFF"
          onPress={goToCreateNewPerson}
        />
      </SafeAreaView>
      </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 10,
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 20,
    bottom: 20,

    backgroundColor: "#1EBBFF"
  },
  emptyDataCard: {
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#e3e3e3"
  },
  emptyDataText: {
    fontSize: 17,
    textAlign: 'center',
    fontWeight: 'bold'
  }
})