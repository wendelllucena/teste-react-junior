import React from 'react'
import ListPeople from '../pages/ListPeople'
import renderer from 'react-test-renderer'


jest.mock('@react-navigation/core')

test('renders LisPeople page correctly', () => {
  const tree = renderer.create(<ListPeople />).toJSON()
  expect(tree).toMatchSnapshot()
})