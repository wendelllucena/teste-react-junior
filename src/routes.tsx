import React from 'react'

import { NavigationContainer } from '@react-navigation/native'
import { Portal, Provider as PaperProvider } from 'react-native-paper'

import { createDrawerNavigator } from '@react-navigation/drawer'
import { createStackNavigator } from '@react-navigation/stack'

import Home from '../src/pages/Home'
import ListPeople from '../src/pages/ListPeople'
import PersonDetails from '../src/pages/PersonDetails'
import Header from './components/Header'
import PersonForm from '../src/pages/PersonForm'

import { CustomDrawerContent } from './components/CustomDrawerContent'

const Drawer = createDrawerNavigator()
const Stack = createStackNavigator()

function Root() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="ListPeople" component={ListPeople} options={{ headerShown: false }} />
      <Stack.Screen name="PersonDetails" component={PersonDetails} options={{ headerShown: false }} />
      <Stack.Screen name="PersonForm" component={PersonForm} options={{ headerShown: false }} />
    </Stack.Navigator>
  )
}

export function DrawerNavigator() {
  return (
    <Drawer.Navigator drawerContent={ props => <CustomDrawerContent {...props} /> }>
      <Drawer.Screen name="Home" component={Home} />
      <Drawer.Screen name="ListPeople" component={Root} />
    </Drawer.Navigator>
  )
}


export default function Routes() {
  return (
    <PaperProvider>
      <Portal>
        <NavigationContainer>
          <DrawerNavigator />
        </NavigationContainer>
      </Portal>
    </PaperProvider>
  )
}