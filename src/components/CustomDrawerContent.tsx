import React, { useState } from 'react'
import { View, StyleSheet } from 'react-native'
import { DrawerContentScrollView, DrawerContentComponentProps } from '@react-navigation/drawer'
import { Divider, Drawer } from 'react-native-paper'


export function CustomDrawerContent(props: DrawerContentComponentProps) {

  const [currentPage, setCurrentPage] = useState('Home')

  const drawerItemTheme = {
    colors: {
      primary: "#1EBBFF"
    }
  }

  return(
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <Drawer.Section style={styles.drawerSection}>
            <Drawer.Item
              active={currentPage === 'Home'}
              icon="home"
              label="Home"
              theme={drawerItemTheme}
              onPress={() => {
                setCurrentPage('Home')
                props.navigation.navigate("Home")              
              }}
            />
            <Divider />
            <Drawer.Item
              active={currentPage === 'ListPeople'}
              icon="account-multiple"
              label="Pessoas"
              theme={drawerItemTheme}
              onPress={() => {
                setCurrentPage('ListPeople')
                props.navigation.navigate("ListPeople")
              }}
            />
          </Drawer.Section>
        </View>
      </DrawerContentScrollView>
    </View>
  )
}


const styles = StyleSheet.create({
  drawerContent: {
    flex: 1
  },
  drawerSection: {
    marginTop: 15
  }
})