import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { DrawerActions } from '@react-navigation/native'
import { useNavigation } from '@react-navigation/core'

import { MaterialIcons } from '@expo/vector-icons'
import { BorderlessButton } from 'react-native-gesture-handler'


interface HeaderProps {
  title: string,
  menu?: boolean,
  rightButton?: React.ReactNode
}


export default function Header(props: HeaderProps) {

  const navigation = useNavigation()

  function toggleDrawer() {
    navigation.dispatch(DrawerActions.toggleDrawer())
  }

  function RenderButton() {
    if (props.menu) {
      return (
        <BorderlessButton onPress={toggleDrawer}>
          <MaterialIcons name="menu" size={26} color="#FFF" />
        </BorderlessButton>
      )
    } else {
      return (
        <BorderlessButton onPress={() => navigation.goBack()}>
          <MaterialIcons name="arrow-back" size={26} color="#FFF" />
        </BorderlessButton>
      )
    }
  }

  return (
    <View style={styles.container}>
      { RenderButton() }
      <Text style={styles.title}>{props.title}</Text>
      <View style={styles.rightButton}>
        { props.rightButton }
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: '#1EBBFF',
    borderBottomWidth: 1,
    borderColor: '#dde3f0',
    paddingTop: 44,

    flexDirection: 'row',
    alignItems: 'center'
  },

  title: {
    color: '#FFF',
    fontSize: 22,
    fontWeight: 'bold',
    marginLeft: 20,
  },
  rightButton: {
    position: 'absolute',
    right: 20,
    top: 46,

    alignItems: 'center',
    justifyContent: 'center'
  }
})