import * as React from 'react'
import { Card, Title } from 'react-native-paper'
import { StyleSheet, View } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/core'

interface PeopleItemProps {
  id: number
  name: string,
  age: number,
  cpf: string
}


export default function PersonCard(props: PeopleItemProps) {

  const navigation = useNavigation()

  function navigateToPeopleDetails() {
    navigation.navigate('PersonDetails', { personId: props.id })
  }

  return (
    <Card style={styles.cardContainer} elevation={1} onPress={navigateToPeopleDetails}>
      <Card.Content>
        <View style={styles.row}>
          <MaterialIcons name="person" size={26} color="#000" />
          <Title style={styles.title}>{props.name}</Title>
        </View>

        <View style={styles.row}>
          <MaterialIcons name="badge" size={26} color="#000" />
          <Title style={styles.title}>{props.cpf}</Title>
        </View>

        <View style={styles.row}>
          <MaterialIcons name="event" size={26} color="#000" />
          <Title style={styles.title}>{props.age}</Title>
        </View>
      </Card.Content>
    </Card>
  )
}

const styles = StyleSheet.create({
  cardContainer: {
    marginBottom: 8,
    padding: 2,
    borderRadius: 6
  },
  cardTitle: {
    paddingRight: 20,
  },
  title: {
    marginLeft: 8,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  }
})