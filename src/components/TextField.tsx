import React from 'react'
import {
  View,
  TextInput,
  Text,
  StyleSheet,
  TextStyle,
  TextInputProps,
} from 'react-native'

import { TextInputMask } from 'react-native-masked-text'


interface Props extends TextInputProps {
  name: string
  label?: string
  labelStyle?: TextStyle
  mask?: string,
}

export default React.forwardRef<any, Props>(
  (props, ref): React.ReactElement => {
    const { label, labelStyle, ...inputProps } = props

    const [onFocus, setFocus] = React.useState(false)

    return (
      <View style={styles.container}>
        {label && <Text style={[styles.label, labelStyle, { color: onFocus ? '#1EBBFF' : '#c0cbd3'  }]}>{label}</Text>}

        {props.mask ? 
          <TextInputMask
            type={"custom"}
            options={{
              mask: props.mask
            }}
            autoCapitalize="none"
            ref={ref}
            style={[styles.input, { borderColor: onFocus ? '#1EBBFF' : '#c0cbd3' }]}
            onFocus={() => setFocus(true)}
            onBlur={() => setFocus(false)}
            {...inputProps}
          /> :
          <TextInput
            autoCapitalize="none"
            ref={ref}
            style={[styles.input, { borderColor: onFocus ? '#1EBBFF' : '#c0cbd3' }]}
            onFocus={() => setFocus(true)}
            onBlur={() => setFocus(false)}
            {...inputProps}
          />
        }
      </View>
    )
  }
)

const styles = StyleSheet.create({
  container: {
    marginVertical: 5,
  },
  input: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 5,
    paddingVertical: 5,
    paddingLeft: 10,
    fontSize: 18,
    height: 50,
    color: '#000000',
  },
  label: {
    paddingVertical: 2,
    fontSize: 16,
    fontWeight: 'bold',
  },
})
