import { Picker } from '@react-native-picker/picker'
import React from 'react'
import {
  View,
  Text,
  StyleSheet,
  TextStyle,
} from 'react-native'

interface Props {
  name: string
  label?: string
  labelStyle?: TextStyle
  mask?: string,
}

export default React.forwardRef<any, Props>(
  (props, ref): React.ReactElement => {
    const { label, labelStyle, ...pickerProps } = props

    return (
      <View style={styles.container}>
        {label && <Text style={styles.label}>{label}</Text>}

        <Picker
          ref={ref}
          style={styles.picker}
          {...pickerProps}
        >
          <Picker.Item label="Masculino" value="Masculino" />
          <Picker.Item label="Feminino" value="Feminino" />
          <Picker.Item label="Outro" value="Outro" />
        </Picker>
      </View>
    )
  }
)

const styles = StyleSheet.create({
  container: {
    marginVertical: 5,
  },
  picker: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#1EBBFF",
    paddingVertical: 5,
    paddingLeft: 10,
    fontSize: 18,
    height: 50,
    color: '#000000',
  },
  label: {
    paddingVertical: 2,
    fontSize: 16,
    fontWeight: 'bold',
    color: "#1EBBFF"
  },
})
