import * as SQLite from 'expo-sqlite'
import {  SQLError, SQLResultSet, SQLTransaction } from 'expo-sqlite'

import { PersonModel } from '../models/person.model'


const db = SQLite.openDatabase('gestaoPessoas.db')

const getDateTime = () => {
  let dateTime = new Date
  return dateTime.toLocaleString()
}

const setupDataBase = () => new Promise((resolve, reject) => {
  const successCallback = (tx: SQLTransaction, results: SQLResultSet) => {
    resolve(results)
  }

  const errorCallback = (tx: SQLTransaction, error: SQLError) => {
    reject(error)
    return false
  }

  db.transaction(
    (tx: SQLTransaction) => {
        tx.executeSql(
          `create table if not exists pessoas (
            id integer primary key autoincrement,
            name text not null,
            age text not null,
            cpf text not null,
            rg text not null,
            gender text not null,
            zip_code text not null,
            address_line text not null,
            number text not null,
            neighborhood text not null,
            city text not null,
            uf text not null,
            created_at text,
            updated_at text
          );`, [],
          successCallback,
          errorCallback
        )
    }
  )
})

const dropTable = () => new Promise((resolve, reject) => {
  const successCallback = (tx: SQLTransaction, results: SQLResultSet) => {
    resolve(results)
  }

  const errorCallback = (tx: SQLTransaction, error: SQLError) => {
    reject(error)
    return false
  }

  db.transaction(
    (tx: SQLTransaction) => {
        tx.executeSql(
          `drop table if exists pessoas;`, [],
          successCallback,
          errorCallback
        )
    }
  )
})

const getPersonList = () => new Promise((resolve, reject) => {
  const successCallback = (tx: SQLTransaction, results: SQLResultSet) => {
    let tempList = []
    for (let i = 0; i < results.rows.length; ++i) {
      tempList.push(results.rows.item(i))
    }
    
    resolve(tempList)
  }

  const errorCallback = (tx: SQLTransaction, error: SQLError) => {
    reject(error)
    return false
  }

  db.transaction(
    (tx: SQLTransaction) => {
      return tx.executeSql(
        'select * from pessoas', [], successCallback, errorCallback
      )
    }
  )
})

const insertPerson = (personData: PersonModel) => new Promise((resolve, reject) => {
  const successCallback = (tx: SQLTransaction, results: SQLResultSet) => {
    resolve(results.rowsAffected)
  }

  const errorCallback = (tx: SQLTransaction, error: SQLError) => {
    reject(error)
    return true
  }

  db.transaction(
    (tx: SQLTransaction) => {
      tx.executeSql(
        `insert into pessoas (name, age, cpf, rg, gender, zip_code, address_line, number, neighborhood, city, uf, created_at)
        values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`,
        [
          personData.name,
          personData.age,
          personData.cpf,
          personData.rg,
          personData.gender,
          personData.zip_code,
          personData.address_line,
          personData.number,
          personData.neighborhood,
          personData.city,
          personData.uf,
          getDateTime()
        ],
        successCallback,
        errorCallback
      )
    }
  )
})

const updatePerson = (personData: any, id: number) => new Promise((resolve, reject) => {
  const successCallback = (tx: SQLTransaction, results: SQLResultSet) => {
    resolve(results.rowsAffected)
  }

  const errorCallback = (tx: SQLTransaction, error: SQLError) => {
    reject(error)
    return true
  }

  db.transaction(
    (tx: SQLTransaction) => {
      tx.executeSql(
        `update pessoas set name=?, age=?, cpf=?, rg=?, gender=?, zip_code=?, address_line=?, number=?, neighborhood=?, city=?, uf=?, updated_at=? where id=?;`,
        [
          personData.name,
          personData.age,
          personData.cpf,
          personData.rg,
          personData.gender,
          personData.zip_code,
          personData.address_line,
          personData.number,
          personData.neighborhood,
          personData.city,
          personData.uf,
          getDateTime(),
          id
        ],
        successCallback,
        errorCallback
      )
    }
  )
})

const deletePersonById = (personId: number) => new Promise((resolve, reject) => {
  const successCallback = (tx: SQLTransaction, results: SQLResultSet) => {
    resolve(results.rowsAffected)
  }

  const errorCallback = (tx: SQLTransaction, error: SQLError) => {
    reject(error)
    return true
  }

  db.transaction(
    (tx: SQLTransaction) => {
      tx.executeSql(
        `delete from pessoas where id=?`,
        [personId],
        successCallback,
        errorCallback
      )
    }
  )
})

const getPersonById = (personId: number) => new Promise((resolve, reject) => {
  const successCallback = (tx: SQLTransaction, results: SQLResultSet) => {
    resolve(results.rows.item(0))
  }

  const errorCallback = (tx: SQLTransaction, error: SQLError) => {
    reject(error)
    return true
  }

  db.transaction(
    (tx: SQLTransaction) => {
      tx.executeSql(
        `select * from pessoas where id=?`,
        [personId],
        successCallback,
        errorCallback
      )
    }
  )
})

export const database = {
  setupDataBase,
  insertPerson,
  getPersonList,
  deletePersonById,
  getPersonById,
  updatePerson,
  dropTable
}