export interface PersonModel {
  name: string,
  age: string,
  cpf: string,
  rg: string,
  gender: string,
  zip_code: string,
  address_line: string,
  number: string,
  neighborhood: string,
  city: string,
  uf: string,
}