
export const cepApi = (zip_code: string) => new Promise((resolve, reject) => {
  fetch(`https://viacep.com.br/ws/${zip_code}/json/`)
    .then(response => response.json())
    .then(address => resolve(address))
    .catch(error => reject(error))
})