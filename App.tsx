import React, { useEffect } from 'react';
import Routes from './src/routes'
import { database } from './src/database/database'

export default function App() {

  useEffect(() => {
    database.setupDataBase().then(data => {
    })
    .catch(error => {
      console.log('create table error', error)
    })
  })

  return (
    <Routes />
  );
}
